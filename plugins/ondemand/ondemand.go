package ondemand

import (
	"context"
	"sync"

	"github.com/digitalocean/godo"
	"go.minekube.com/brigodier"
	"go.minekube.com/common/minecraft/component"
	"go.minekube.com/gate/pkg/command"
	"go.minekube.com/gate/pkg/edition/java/proxy"
)

const (
	PluginName = "minecraft-on-demand"

	// const messages
	statusMsgFailedToRefresh = "failed to refresh server info, try again."
	statusMsgServerReady     = "server is up, join to play!"
	statusMsgServerDown      = "server is down, join to bring it up"
	statusMsgServerStarting  = "server is starting, please wait and refresh"
)

type Config struct {
	// ServerConfigs
	//   -> map[droplet_name/server_name]ServerConfig
	ServerConfigs map[string]ServerConfig `yaml:"server_configs"`

	// digitalocean authentication token
	DOToken string `yaml:"do_token"`
}

// configs for DigitalOcean droplet
type ServerConfig struct {
	// name of the server in DigitalOcean,
	Name     string `yaml:"name"`
	HostName string `yaml:"host_name"` // currently not used

	Region            string `yaml:"region"`
	ImageID           int    `yaml:"image_id"`
	SSHKeyFingerprint string `yaml:"ssh_key_fingerprint"`
	VolumeID          string `yaml:"volume_id"`
	DropletSize       string `yaml:"droplet_size"`
}

type onDemand struct {
	proxy    *proxy.Proxy
	config   Config
	provider Provider

	// map[name]ServerConfig
	servers map[string]ServerConfig

	registeredServers map[string]proxy.RegisteredServer

	sync.RWMutex
}

func Register(c Config) {
	proxy.Plugins = append(proxy.Plugins, proxy.Plugin{
		Name: PluginName,
		Init: func(ctx context.Context, proxy *proxy.Proxy) error {
			return newOnDemandProxy(proxy, c).init()
		},
		// Init: func(proxy *proxy.Proxy) error {
		// 	return newOnDemandProxy(proxy, c).init()
		// },
	})
}

func newOnDemandProxy(p *proxy.Proxy, c Config) *onDemand {
	return &onDemand{
		proxy:             p,
		config:            c,
		servers:           make(map[string]ServerConfig),
		registeredServers: make(map[string]proxy.RegisteredServer),
	}
}

func (od *onDemand) init() error {
	// register user commands
	// register events
	od.registerCommands()

	// initialise servers provider (digital ocean client, etc.)
	od.servers = od.config.ServerConfigs
	od.provider.Client = godo.NewFromToken(od.config.DOToken)

	return nil
}

func (od *onDemand) registerCommands() {
	od.registerJoinCommand()
	// test commands
	od.proxy.Command().Register(brigodier.Literal("test").Executes(command.Command(func(c *command.Context) error {
		player, ok := c.Source.(proxy.Player)
		if !ok {
			return nil
		}

		player.TabList().SetHeaderFooter(
			&component.Text{Content: "Test header"},
			&component.Text{Content: "Test footer"},
		)

		player.SendActionBar(
			&component.Text{Content: "Test footer"},
		)

		return nil
	})))
}
