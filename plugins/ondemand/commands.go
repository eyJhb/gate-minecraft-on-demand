package ondemand

import (
	"context"
	"fmt"
	"net"
	"time"

	"github.com/rs/zerolog/log"
	"go.minekube.com/brigodier"
	"go.minekube.com/gate/pkg/command"
	"go.minekube.com/gate/pkg/edition/java/proxy"
)

type serverState uint8

const (
	serverDown serverState = iota
	serverCreated
	serverStarted
	serverRunning
)

func (ss serverState) String() string {
	switch ss {
	case serverDown:
		return "down"
	case serverCreated:
		return "created"
	case serverStarted:
		return "started"
	case serverRunning:
		return "running"
	}

	return "unknown"
}

const (
	// default minecraft port
	mcDefaultPort = 25565

	// default creation timeout
	serverCreateTimeout = 90 * time.Second
)

const (
	// user errors
	userErrServerNotExists        = "server does not exists"
	userErrServerUnableFetchState = "unable to fetch state of the server, please try again"
	userErrServerFailCreate       = "unable to start server, please try again"
	userErrFailedConnectToServer  = "unable to connect to server, please try again"

	// user messages
	userMsgStartingServer     = "created server, waiting for it to start"
	userMsgConnectingToServer = "connecting you to the server!"
	userMsgConnectedToServer  = "successfully connected you to the server!"
	userMsgServerNotRunnig    = "the minecraft server is not running yet, please try again."
	// userMsgStartingServer = "server status"
)

func (od *onDemand) registerJoinCommand() {
	od.proxy.Command().Register(brigodier.Literal("join").Then(
		// Adds message argument as in "/join <server>"
		brigodier.Argument("server", brigodier.StringPhrase).
			// Adds completion suggestions as in "/join [suggestions]"
			Suggests(command.SuggestFunc(func(
				c *command.Context,
				b *brigodier.SuggestionsBuilder,
			) *brigodier.Suggestions {
				// add all server names to suggestions
				for serverName := range od.servers {
					b.Suggest(serverName)
				}
				return b.Build()
			})).
			// Executed when running "/join <server>"
			Executes(command.Command(func(c *command.Context) error {
				player, ok := c.Source.(proxy.Player)
				if !ok {
					log.Info().Msg("something triggered join but is not a player")
					return nil
				}

				return od.triggerJoin(c, player)
			})),
	))
}

func (od *onDemand) triggerJoin(c *command.Context, player proxy.Player) error {
	// extract the server that the player wants to join, from the argument
	serverName := c.String("server")

	// ensure that the server chosen is valid
	serverConfig, ok := od.servers[serverName]
	if !ok {
		sendPlayerError(player, userErrServerNotExists)
		return nil
	}

	sendPlayerMsg(player, fmt.Sprintf("Trying to connect you to server %s, please wait...", serverName))

	// check if server is up and running
	state, err := od.getServerState(c.Context, serverConfig)
	if err != nil {
		sendPlayerError(player, userErrServerUnableFetchState)
		return err
	}

	if state == serverDown {
		// server is down, creating it
		sendPlayerMsg(player, userMsgStartingServer)
		if err := od.serverCreate(c.Context, serverConfig, serverCreateTimeout); err != nil {
			fmt.Println("error", err)
			log.Error().Err(err).Msg("failed to start server")
			sendPlayerError(player, userErrServerFailCreate)
			return nil
		}
		sendPlayerSuccess(player, userMsgStartingServer)
	}

	go od.updatePlayerStartingProcess(c.Context, player, serverConfig)

	return nil

}

func (od *onDemand) updatePlayerStartingProcess(parentctx context.Context, player proxy.Player, serverConfig ServerConfig) error {
	var isRunning bool
	for i := 0; i < 60; i++ {
		time.Sleep(5 * time.Second)

		// make context
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

		// get server state
		state, err := od.getServerState(ctx, serverConfig)
		if err != nil {
			sendPlayerMsg(player, userErrServerUnableFetchState)
			cancel()
			log.Error().Err(err).Msg("failed to get server state inside updatePlayerStartingProcess")
			return err
		}
		cancel()

		if state != serverRunning {
			sendPlayerMsg(player, fmt.Sprintf("server is %s", state.String()))
			continue
		}

		isRunning = true
		break
	}

	if !isRunning {
		sendPlayerSuccess(player, userMsgServerNotRunnig)
		return nil
	}

	// server is now running, connect the player
	od.RLock()
	defer od.RUnlock()

	// TODO: unsure if parent context is the correct thing here
	sendPlayerMsg(player, userMsgConnectingToServer)

	// new context for connecting
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	success := player.CreateConnectionRequest(od.registeredServers[serverConfig.Name]).ConnectWithIndication(ctx)
	if success {
		sendPlayerSuccess(player, userMsgConnectedToServer)
	} else {
		sendPlayerError(player, userErrFailedConnectToServer)
	}

	return nil
}

func (od *onDemand) serverCreate(parentctx context.Context, serverConfig ServerConfig, timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	return od.provider.Create(ctx, serverConfig)
}

func (od *onDemand) getServerState(ctx context.Context, serverConfig ServerConfig) (serverState, error) {
	dropletInfo, err := od.provider.GetInfo(ctx, serverConfig)
	if err != nil {
		return serverDown, err
	}

	// update registered server information to nil
	// if the server is not active
	if dropletInfo.State != DropletStateActive {
		od.Lock()
		if registeredServer, ok := od.registeredServers[serverConfig.Name]; ok {
			if registeredServer != nil {
				od.proxy.Unregister(registeredServer.ServerInfo())
			}
			delete(od.registeredServers, serverConfig.Name)
		}
		od.Unlock()
	}

	switch dropletInfo.State {
	case DropletStateNotCreated:
		return serverDown, nil
	case DropletStateNew:
		return serverCreated, nil
	case DropletStateActive:
		// do nothing, as we want to continue on from this
	case DropletStateOff:
		return serverDown, nil
	default:
		return serverDown, nil
	}

	// droplet is ensured to be active here
	// because of the switch statement

	// check if we can connect to server, without any errors, if not, server is started but not running MC yet
	conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", dropletInfo.IPv4, mcDefaultPort), 5*time.Second)
	if err != nil {
		return serverStarted, nil
	}
	defer conn.Close()

	// lock so the registered server can be added
	od.Lock()
	defer od.Unlock()

	// check if server is already added
	if _, ok := od.registeredServers[serverConfig.Name]; ok {
		return serverRunning, nil
	}

	// add server as registered server, as it was not already added

	// first resolve the addr + port
	tmpAddrPort, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%d", dropletInfo.IPv4, mcDefaultPort))
	if err != nil {
		return serverRunning, err
	}

	// add it using our proxy
	registeredServer, err := od.proxy.Register(proxy.NewServerInfo(serverConfig.Name, tmpAddrPort))
	if err != nil {
		return serverRunning, err
	}

	// add to our map
	od.registeredServers[serverConfig.Name] = registeredServer

	// server is started and minecraft is running
	return serverRunning, nil
}
