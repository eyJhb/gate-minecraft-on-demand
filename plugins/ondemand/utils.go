package ondemand

import (
	"go.minekube.com/common/minecraft/color"
	"go.minekube.com/common/minecraft/component"
	"go.minekube.com/gate/pkg/edition/java/proxy"
)

func sendPlayerMsg(player proxy.Player, msg string) {
	player.SendMessage(&component.Text{
		Content: msg,
		S:       component.Style{Color: color.White, Bold: component.True},
	})
}

func sendPlayerError(player proxy.Player, msg string) {
	player.SendMessage(&component.Text{
		Content: msg,
		S:       component.Style{Color: color.Red, Bold: component.True},
	})
}

func sendPlayerSuccess(player proxy.Player, msg string) {
	player.SendMessage(&component.Text{
		Content: msg,
		S:       component.Style{Color: color.Green, Bold: component.True},
	})
}
