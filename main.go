package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/go-logr/logr"
	"github.com/go-logr/zerologr"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/eyJhb/gate-minecraft-on-demand/plugins/ondemand"
	jconfig "go.minekube.com/gate/pkg/edition/java/config"
	"go.minekube.com/gate/pkg/gate"
	gateconfig "go.minekube.com/gate/pkg/gate/config"
	"gopkg.in/yaml.v2"
)

const (
	configDirPostfix = "/minecraft-on-demand/config.yml"
)

type config struct {
	OnDemandConfig ondemand.Config `yaml:"on_demand_config"`
	Lobby          LobbyConfig     `yaml:"lobby"`

	// will be used to set any unset value in OnDemandConfig for servers
	DOToken string `yaml:"do_token"`

	// is used when setting up the Gate proxy with modern forwarding
	VelocitySecret string `yaml:"velocity_secret"`

	// status information
	ServerListStatus *jconfig.Status `yaml:"server_list_status"`

	// bind information
	BindAdress string `yaml:"bind_address"`
	BindPort   int    `yaml:"bind_port"`
}

type LobbyConfig struct {
	Address string `yaml:"address"`
	Port    int    `yaml:"port"`
}

var (
	flagConfig = flag.String("config", "", "where to read the configuration from, if empty defaults to $XDG_CONFIG_HOME/minecraft-on-demand/config.yml else $HOME/.config/minecraft-on-demand/config.yml")

	// auth token file
	flagAuthTokenFile = flag.String("auth-token-file", "", "specifies the file where the authtoken to digitalocean can be read")

	// velocity secret file
	flagVelocitySecretFile = flag.String("velocity-secret-file", "", "specifies the file where the shared secret for velocity forwarding can be read")

	// bind information
	flagBind = flag.String("bind", "0.0.0.0", "specifies the host to bind on")
	flagPort = flag.Int("port", 25565, "specifies the port to bind on")
)

func main() {
	flag.Parse()

	if err := run(); err != nil {
		log.Panic().Err(err).Msg("failed to start proxy")
	}
}

func run() error {
	// setup logger with zerolog
	zl := zerolog.New(os.Stderr)
	var log logr.Logger = zerologr.New(&zl)

	// get config
	conf, err := getConfig()
	if err != nil {
		return err
	}

	// get gate configuration
	gconf := getGateConfig(conf)

	// register plugns
	ondemand.Register(conf.OnDemandConfig)

	// start proxy
	ctx := context.Background()
	ctx = logr.NewContext(ctx, log)
	return gate.Start(ctx, gate.WithConfig(gconf))
}

func getConfig() (config, error) {
	configdir := *flagConfig
	if configdir == "" {
		tmpvar, err := os.UserConfigDir()
		if err != nil {
			return config{}, err
		}

		configdir = tmpvar + configDirPostfix
	}

	// try to read the configuration file file
	data, err := ioutil.ReadFile(configdir)
	if err != nil {
		return config{}, err
	}

	var conf config
	if err := yaml.Unmarshal(data, &conf); err != nil {
		return config{}, err
	}

	// ensure there is a lobby server
	if conf.Lobby.Address == "" || conf.Lobby.Port == 0 {
		return config{}, errors.New("Please set lobby address and port")
	}

	// read the authentication token for digital ocean
	if *flagAuthTokenFile != "" {
		authTokenBytes, err := ioutil.ReadFile(*flagAuthTokenFile)
		if err != nil {
			return config{}, err
		}
		conf.DOToken = string(authTokenBytes)
	}
	conf.OnDemandConfig.DOToken = conf.DOToken

	// read velocity secret from file
	if *flagVelocitySecretFile != "" {
		velocitySecretBytes, err := ioutil.ReadFile(*flagVelocitySecretFile)
		if err != nil {
			return config{}, err
		}
		conf.VelocitySecret = string(velocitySecretBytes)
	}

	// set status if not set
	if conf.ServerListStatus == nil {
		conf.ServerListStatus = &gateconfig.DefaultConfig.Editions.Java.Config.Status
	}

	// override bindadress/port if specified
	if *flagBind != "" {
		conf.BindAdress = *flagBind
	}
	if *flagPort != 0 {
		conf.BindPort = *flagPort
	}

	// set default configs for servers
	for serverName, serverConfig := range conf.OnDemandConfig.ServerConfigs {
		if serverConfig.Region == "" {
			return conf, fmt.Errorf("field %s not filled for server %s", "region", serverName)
		} else if serverConfig.ImageID == 0 {
			return conf, fmt.Errorf("field %s not filled for server %s", "image_id", serverName)
		} else if serverConfig.SSHKeyFingerprint == "" {
			return conf, fmt.Errorf("field %s not filled for server %s", "ssh_key_fingerprint", serverName)
		} else if serverConfig.VolumeID == "" {
			return conf, fmt.Errorf("field %s not filled for server %s", "volume_id", serverName)
		} else if serverConfig.DropletSize == "" {
			return conf, fmt.Errorf("field %s not filled for server %s", "droplet_size", serverName)
		}

		if serverConfig.Name == "" {
			serverConfig.Name = serverName
		}

		conf.OnDemandConfig.ServerConfigs[serverName] = serverConfig
	}

	return conf, nil
}

func getGateConfig(conf config) gateconfig.Config {
	// base on the default config, and then owerride options
	gconf := gateconfig.DefaultConfig
	gconf.Editions.Java.Config.Bind = fmt.Sprintf("%s:%d", conf.BindAdress, conf.BindPort)
	gconf.Editions.Java.Config.Forwarding = jconfig.Forwarding{Mode: jconfig.VelocityForwardingMode, VelocitySecret: conf.VelocitySecret}
	gconf.Editions.Java.Config.Servers = map[string]string{"lobby": fmt.Sprintf("%s:%d", conf.Lobby.Address, conf.Lobby.Port)}
	gconf.Editions.Java.Config.Try = []string{"lobby"}

	return gconf
}
